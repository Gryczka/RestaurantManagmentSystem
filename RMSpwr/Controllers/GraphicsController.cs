﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RMSpwr.Models.Entities.Graphic;
using RMSpwr.Models.Infrastructure;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using RMSpwr.Models.Entities;
using Newtonsoft.Json;

namespace RMSpwr.Controllers
{
    [Produces("application/json")]
    [Route("api/Graphics")]
    public class GraphicsController : Controller
    {
        private OrderContext _context;
        private ILogger _logger;
        public GraphicsController(ILogger<GraphicsController> logger, OrderContext context)
        {
            _context = context;
            _logger = logger;
        }

        // GET api/Graphics
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<GraphicInfo> graphicInfos = _context.GraphicInfos.ToList();
            List<Group> groups = _context.Groups.ToList();
            var groupsObjectified = groups.Select(q => new
            {
                Code = q.Code,
                Name = q.Name,
                Count_of_person = q.Count_of_person
            });
            List<Shift> shifts = _context.Shifts.ToList();
            var shiftsObjectified = shifts.Select(q => new ShiftsJson
            {
                Code = q.Code,
                Description = q.Description,
                Duration = 8
            }).ToList();
            List<Worker> workers = _context.Workers.ToList();
            var workersObjectified = workers.Select(q => new WorkersJson
            {
                Name = q.Name,
                Group = _context.Groups.First(g => g.GroupID == q.GroupID).Code,
                Working_time = new WorkingTime { Time = q.WorkingTime },
                Position = "",
                //Position = q.Position.Name
            }).ToList();
            List<Position> positions = _context.Positions.ToList();
            var positionsObjectified = positions.Select(q => new PositionsJson
            {
                Name = q.Name,
                Group = _context.Groups.First(g => g.GroupID == q.GroupID).Code,
                Count_of_person = q.Count_of_person
            }).ToList();
            var data = new GraphicInfoJson
            {
                Graphic_info = graphicInfos.First(),
                Groups = groups,
                Shifts = shiftsObjectified,
                Workers = workersObjectified,
                Positions = positionsObjectified
            };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var result = client.UploadString("http://localhost:8000/graphic_generator/graphic_info", "POST", json.ToString());
                return Ok(result);
            }
        }

        // POST api/Graphics/201704
        [HttpPost("{date}")]
        public async Task<IActionResult> Generate([FromRoute] string date)
        {
            var data = new GenerateGraphicJson
            {
                graphic_id = _context.GraphicInfos.ToList().First().Graphic_id,
                date = date
            };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(data, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                var result = client.UploadString("http://localhost:8000/graphic_generator/generate", "POST", json.ToString());

                var days = JsonConvert.DeserializeObject<DaysList>(result);

                foreach (var day in days.Days)
                {
                    _context.Days.Add(day);
                }
                await _context.SaveChangesAsync();
                return Ok("SUCCESS");
            }
        }

        // GET api/Graphics/Days
        [HttpGet("Days")]
        public async Task<IActionResult> ListDays()
        {
            List<Day> days = _context.Days.ToList();

            return Ok(days.Select(q => new
            {
                DayID = q.DayID,
                Date = q.Date,
                Position = q.Position,
                Shift = q.Shift,
                Worker_name = q.Worker_name
            }));
        }

        // DELETE: api/Graphics/Days
        [HttpDelete("Days")]
        public async Task<IActionResult> DeleteDays()
        {
            foreach (var day in _context.Days.ToList())
            {
                _context.Days.Remove(day);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }

        // GET api/Graphics/Workers
        [HttpGet("Workers")]
        public async Task<IActionResult> ListWorkers()
        {
            List<Worker> workers = _context.Workers.ToList();

            return Ok(workers.Select(q => new
            {
                WorkerID = q.WorkerID,
                Name = q.Name,
                //Group = q.Group,
                WorkingTime = q.WorkingTime,
                //Position = q.Position
            }));
        }

        // DELETE: api/Graphics/Workers
        [HttpDelete("Workers")]
        public async Task<IActionResult> DeleteWorkers()
        {
            foreach (var worker in _context.Workers.ToList())
            {
                _context.Workers.Remove(worker);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }

        // GET api/Graphics/Shifts
        [HttpGet("Shifts")]
        public async Task<IActionResult> ListShifts()
        {
            List<Shift> shifts = _context.Shifts.ToList();

            return Ok(shifts.Select(q => new
            {
                ShiftID = q.ShiftID,
                Code = q.Code,
                Description = q.Description,
                From = q.From,
                To = q.To,
            }));
        }

        // GET api/Graphics/UpsertShift
        [HttpGet("UpsertShift")]
        public async Task<IActionResult> UpsertShift()
        {

            var shifts = new Shift[]
            {
                    new Shift{ ShiftID = Guid.NewGuid(), Code = "Zmiana1", Description = "24-8", From = new TimeSpan(0, 0, 0), To = new TimeSpan(8, 0, 0) },
                    new Shift{ ShiftID = Guid.NewGuid(), Code = "Zmiana2", Description = "8-16", From = new TimeSpan(8, 0, 0), To = new TimeSpan(16, 0, 0) },
                    new Shift{ ShiftID = Guid.NewGuid(), Code = "Zmiana3", Description = "16-24", From = new TimeSpan(16, 0, 0), To = new TimeSpan(23, 59, 59) },
            };
            
            foreach (var shift in shifts)
            {
                _context.Shifts.Add(shift);
            }
           
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }

        // DELETE: api/Graphics/Shifts
        [HttpDelete("Shifts")]
        public async Task<IActionResult> DeleteShifts()
        {
            foreach (var shift in _context.Shifts.ToList())
            {
                _context.Shifts.Remove(shift);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }

        // GET api/Graphics/Positions
        [HttpGet("Positions")]
        public async Task<IActionResult> ListPositions()
        {
            List<Position> positions = _context.Positions.ToList();

            return Ok(positions.Select(q => new
            {
                PositionID = q.PositionID,
                Name = q.Name,
                GroupID = q.GroupID,
                Count_of_person = q.Count_of_person,
            }));
        }

        // GET api/Graphics/Groups
        [HttpGet("Groups")]
        public async Task<IActionResult> ListGroups()
        {
            List<Group> groups = _context.Groups.ToList();

            return Ok(groups.Select(q => new
            {
                GroupID = q.GroupID,
                Code = q.Name,
                Name = q.Name,
                Count_of_person = q.Count_of_person,
            }));
        }

        // GET api/Graphics/PositionsInGroup/groupID
        [HttpGet("PositionsInGroup/{groupID}")]
        public async Task<IActionResult> ListPositionsInGroup(Guid GroupID)
        {
            List<Position> positions = _context.Positions.Where(p => p.GroupID == GroupID).ToList();

            return Ok(positions.Select(q => new
            {
                PositionID = q.PositionID,
                Name = q.Name,
                GroupID = q.GroupID,
                Count_of_person = q.Count_of_person,
            }));
        }


        // GET api/Graphics/Worker/{position}
        [HttpGet("Worker/{position}")]
        public async Task<IActionResult> WorkerOnPosition(string position)
        {
            var time = DateTime.Now.TimeOfDay;
            var shift = _context.Shifts.First(s => s.From <= time && s.To > time);
            var days = _context.Days.Where(d => d.Date == DateTime.Today.ToString("yyyy-MM-dd")).Where(d => d.Position == position).First(d => d.Shift == shift.Code);

            var worker = _context.Workers.Where(d => d.Name == days.Worker_name);
            return Ok(worker.Select(q => new
            {
                Worker_name = q.Name
            }));
        }

        // DELETE: api/Graphics/Positions
        [HttpDelete("Positions")]
        public async Task<IActionResult> DeletePositions()
        {
            foreach (var position in _context.Positions.ToList())
            {
                _context.Positions.Remove(position);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }

        //private readonly OrderContext _context;

        //public GraphicsController(OrderContext context)
        //{
        //    _context = context;
        //}

        //// GET: api/Graphics
        //[HttpGet]
        //public IEnumerable<GraphicInfo> GetGraphicInfos()
        //{
        //    return _context.GraphicInfos;
        //}

        //// GET: api/Graphics/5
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetGraphicInfo([FromRoute] string id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var graphicInfo = await _context.GraphicInfos.SingleOrDefaultAsync(m => m.Graphic_id == id);

        //    if (graphicInfo == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(graphicInfo);
        //}

        //// PUT: api/Graphics/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutGraphicInfo([FromRoute] string id, [FromBody] GraphicInfo graphicInfo)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != graphicInfo.Graphic_id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(graphicInfo).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!GraphicInfoExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/Graphics
        //[HttpPost]
        //public async Task<IActionResult> PostGraphicInfo([FromBody] GraphicInfo graphicInfo)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _context.GraphicInfos.Add(graphicInfo);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetGraphicInfo", new { id = graphicInfo.Graphic_id }, graphicInfo);
        //}

        //// DELETE: api/Graphics/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteGraphicInfo([FromRoute] string id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var graphicInfo = await _context.GraphicInfos.SingleOrDefaultAsync(m => m.Graphic_id == id);
        //    if (graphicInfo == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.GraphicInfos.Remove(graphicInfo);
        //    await _context.SaveChangesAsync();

        //    return Ok(graphicInfo);
        //}

        //private bool GraphicInfoExists(string id)
        //{
        //    return _context.GraphicInfos.Any(e => e.Graphic_id == id);
        //}
    }
}