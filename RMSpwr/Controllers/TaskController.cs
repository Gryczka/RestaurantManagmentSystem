﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMSpwr.Models.Infrastructure;
using Microsoft.Extensions.Logging;
using RMSpwr.Models.Entities;
using RMSpwr.Models.Helpers;

namespace RMSpwr.Controllers
{
    [Produces("application/json")]
    [Route("api/Task")]
    public class TaskController : Controller
    {
        private OrderContext _context;
        private ILogger _logger;
        public TaskController(ILogger<TaskController> logger, OrderContext context)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Task/Drink
        [HttpGet("Drink")]
        public async Task<IActionResult> DrinkList()
        {
            List<DrinkTask> drinkTasks = _context.DrinkTasks.ToList();

            return Ok(drinkTasks.Select(q => new
            {
                DrinkTaskID = q.DrinkTaskID,
                Drink = _context.Drinks.First(d => d.Id == q.DrinkID),
                Order = _context.Orders.First(o => o.OrderID == q.OrderID),
                Worker = _context.Workers.First(w => w.WorkerID == q.WorkerID),
                Status = q.Status
            }));
        }

        // GET: api/Task/Dish
        [HttpGet("Dish")]
        public async Task<IActionResult> DishList()
        {
            List<DishTask> dishTasks = _context.DishTasks.ToList();

            return Ok(dishTasks.Select(q => new
            {
                DishTaskID = q.DishTaskID,
                Drink = _context.Dishes.First(d => d.DishID == q.DishID),
                Order = _context.Orders.First(o => o.OrderID == q.OrderID),
                Worker = _context.Workers.First(w => w.WorkerID == q.WorkerID),
                Status = q.Status
            }));
        }

        // GET: api/Task/DishID/{id}
        [HttpGet("DishID/{dishID}")]
        public async Task<IActionResult> DishID(Guid dishID)
        {
            DishTask q = null;
            try
            {
                q = _context.DishTasks.Single(d => d.DishTaskID == dishID);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound(new { status = "notFound" });
            }

            return Ok(new
            {
                DishTaskID = q.DishTaskID,
                Dish = _context.Dishes.First(d => d.DishID == q.DishID),
                Order = _context.Orders.First(o => o.OrderID == q.OrderID),
                Worker = _context.Workers.First(w => w.WorkerID == q.WorkerID),
                Status = q.Status
            });
        }

        // PUT: api/Task/Dish/{id}/{status}
        [HttpPut("Dish/{DishID}/{Status}")]
        public async Task<IActionResult> DishStatusChange([FromRoute] Guid DishID, [FromRoute] int Status)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dishTask = _context.DishTasks.First(d => d.DishTaskID == DishID);
                    if (dishTask == null) return NotFound(new { status = "notFound" });
                    dishTask.Status = (Status)Status;
                    _context.DishTasks.Attach(dishTask);
                    _context.Entry(dishTask).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return Ok(new { status = "updated" });
                }
                else
                {
                    return BadRequest(new { status = "failure" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound(new { status = "notFound" });
            }
        }

        // GET: api/Task/DrinkID/{id}
        [HttpGet("DrinkID/{drinkID}")]
        public async Task<IActionResult> DrinkID(Guid drinkID)
        {
            DrinkTask q = null;
            try
            {
                q = _context.DrinkTasks.Single(d => d.DrinkTaskID == drinkID);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound(new { status = "notFound" });
            }

            return Ok(new
            {
                DishTaskID = q.DrinkTaskID,
                Drink = _context.Drinks.First(d => d.Id == q.DrinkID),
                Order = _context.Orders.First(o => o.OrderID == q.OrderID),
                Worker = _context.Workers.First(w => w.WorkerID == q.WorkerID),
                Status = q.Status
            });
        }

        // PUT: api/Task/Drink/{id}/{status}
        [HttpPut("Drink/{DrinkID}/{Status}")]
        public async Task<IActionResult> DrinkStatusChange([FromRoute] Guid DrinkID, [FromRoute] int Status)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var drinkTask = _context.DrinkTasks.First(d => d.DrinkTaskID == DrinkID);
                    if (drinkTask == null) return NotFound(new { status = "notFound" });
                    drinkTask.Status = (Status)Status;
                    _context.DrinkTasks.Attach(drinkTask);
                    _context.Entry(drinkTask).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return Ok(new { status = "updated" });
                }
                else
                {
                    return BadRequest(new { status = "failure" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound(new { status = "notFound" });
            }
        }

        // DELETE: api/Task/Drink
        [HttpDelete("Drink")]
        public async Task<IActionResult> DrinkDelete()
        {
            foreach (var task in _context.DrinkTasks.ToList())
            {
                _context.DrinkTasks.Remove(task);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }

        // DELETE: api/Task/Dish
        [HttpDelete("Dish")]
        public async Task<IActionResult> DishDelete()
        {
            foreach (var task in _context.DishTasks.ToList())
            {
                _context.DishTasks.Remove(task);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }

        // GET: api/Task/Drink/12
        [HttpGet("Drink/{WorkerID}")]
        public async Task<IActionResult> DrinkListForWorker(Guid WorkerID)
        {
            List<DrinkTask> drinkTasks = _context.DrinkTasks.Where(d => d.WorkerID == WorkerID).ToList();

            return Ok(drinkTasks.Select(q => new
            {
                DrinkTaskID = q.DrinkTaskID,
                Drink = _context.Drinks.First(d => d.Id == q.DrinkID),
                Order = _context.Orders.First(o => o.OrderID == q.OrderID),
                Worker = _context.Workers.First(w => w.WorkerID == q.WorkerID),
                Status = q.Status
            }));
        }

        // GET: api/Task/DrinkForWorkerWithStatus/12/0
        [HttpGet("DrinkForWorkerWithStatus/{WorkerID}/{Status}")]
        public async Task<IActionResult> DrinkForWorkerWithStatus(Guid WorkerID, int Status)
        {
            List<DrinkTask> drinkTasks = _context.DrinkTasks.Where(d => d.WorkerID == WorkerID).Where(d => d.Status == (Status)Status).ToList();

            return Ok(drinkTasks.Select(q => new
            {
                DrinkTaskID = q.DrinkTaskID,
                Drink = _context.Drinks.First(d => d.Id == q.DrinkID),
                Order = _context.Orders.First(o => o.OrderID == q.OrderID),
                Worker = _context.Workers.First(w => w.WorkerID == q.WorkerID),
                Status = q.Status
            }));
        }


        // GET: api/Task/DishForWorkerWithStatus/12/0
        [HttpGet("DishForWorkerWithStatus/{WorkerID}/{Status}")]
        public async Task<IActionResult> DishListForWorker(Guid WorkerID, int Status)
        {
            List<DishTask> dishTasks = _context.DishTasks.Where(d => d.WorkerID == WorkerID).Where(d => d.Status == (Status)Status).ToList();

            return Ok(dishTasks.Select(q => new
            {
                DishTaskID = q.DishTaskID,
                Drink = _context.Dishes.First(d => d.DishID == q.DishID),
                Order = _context.Orders.First(o => o.OrderID == q.OrderID),
                Worker = _context.Workers.First(w => w.WorkerID == q.WorkerID),
                Status = q.Status
            }));
        }
    }
}