﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RMSpwr.Models.Infrastructure;
using RMSpwr.Models.Entities;
using RMSpwr.Models.Helpers;

namespace RMSpwr.Controllers
{
    [Produces("application/json")]
    [Route("api/Drink")]
    public class DrinkController : Controller
    {
        private OrderContext _context;
        private ILogger _logger;
        public DrinkController(ILogger<DrinkController> logger, OrderContext context)
        {
            _context = context;
            _logger = logger;
        }
        // GET: api/Drink
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<Drink> drinks = _context.Drinks.ToList();
            var drinksObjectified = drinks.Select(q => new
            {
                Id = q.Id,
                Tasks = q.Tasks,
                Name = q.Name,
                Amount = q.Amount,
                Components = q.Components,
                Description = q.Description,
                AdditionalInformation = q.AdditionalInformation,
                Position = q.Position
            });
            return Ok(drinksObjectified);
        }

        // DELETE: api/Drink
        [HttpDelete]
        public async Task<IActionResult> Delete()
        {
            foreach (var drink in _context.Drinks.ToList())
            {
                _context.Drinks.Remove(drink);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }
    }
}