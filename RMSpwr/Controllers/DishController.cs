﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMSpwr.Models.Infrastructure;
using Microsoft.Extensions.Logging;
using RMSpwr.Models.Entities;
using RMSpwr.Models.Helpers;

namespace RMSpwr.Controllers
{
    [Produces("application/json")]
    [Route("api/Dish")]
    public class DishController : Controller
    {
        private OrderContext _context;
        private ILogger _logger;
        public DishController(ILogger<DishController> logger, OrderContext context)
        {
            _context = context;
            _logger = logger;
        }

        // GET api/Dish
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<Dish> dishes = _context.Dishes.ToList();
            var dishesObjectified = dishes.Select(q => new
            {
                DishID = q.DishID,
                Tasks = q.Tasks,
                Name = q.Name,
                Amount = q.Amount,
                Components = q.Components,
                Description = q.Description,
                AdditionalInformation = q.AdditionalInformation,
                Position = q.Position
            });
            return Ok(dishesObjectified);
        }

        // GET api/Dish/Compontents
        [HttpGet("Components")]
        public async Task<IActionResult> ComponentsList()
        {
            List<Component> components = await _context.LazyLoadComponents();
            var componentsObjectified = components.Select(q => new
            {
                Id = q.Id,
                Element = q.Element,
            });
            return Ok(componentsObjectified);
        }

        // DELETE: api/Dish
        [HttpDelete]
        public async Task<IActionResult> Delete()
        {
            foreach (var dish in _context.Dishes.ToList())
            {
                _context.Dishes.Remove(dish);
            }
            await _context.SaveChangesAsync();
            return Ok("SUCCESS");
        }
    }
}