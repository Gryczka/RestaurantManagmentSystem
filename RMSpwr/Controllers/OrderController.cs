﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMSpwr.Models.Infrastructure;
using Microsoft.Extensions.Logging;
using RMSpwr.Models.Entities;
using RMSpwr.Models.Concrete;
using RMSpwr.Models;
using Microsoft.EntityFrameworkCore;
using RMSpwr.Models.Helpers;

namespace RMSpwr.Controllers
{
    [Produces("application/json")]
    [Route("api/Order")]
    public class OrderController : Controller
    {
        private OrderContext _context;
        private ILogger _logger;
        public OrderController(ILogger<OrderController> logger, OrderContext context)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Orders
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<Order> orders = await _context.LazyLoadOrders();
            

            return Ok(orders.Select(q => {

                return new
                {
                    OrderID = q.OrderID,
                    Table = q.Table,
                    DishTasks = q.DishTasks.First(dt => _context.Dishes.First(d => d.DishID == dt.DishID) != null),
                    DrinkTasks = q.DrinkTasks.First(dt => _context.Drinks.First(d => d.Id == dt.DrinkID) != null),
                    IsPaid = q.IsPaid,
                    Amount = q.Amount,
                    Status = q.Status,
                    PaymentMethod = q.PaymentMethod.ToString()
                };
             }));

        }

        // GET api/Order/{id}
        [HttpGet("{OrderID}")]
        public async Task<IActionResult> Read(Guid OrderID)
        {
            Order q = null;
            try
            {
                q = await _context.LazyLoadOrder(OrderID);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound(new { status = "notFound" });
            }
            return Ok(new
            {
                OrderID = q.OrderID,
                Table = q.Table,
                DishTasks = q.DishTasks.First(dt => _context.Dishes.First(d => d.DishID == dt.DishID) != null),
                DrinkTasks = q.DrinkTasks.First(dt => _context.Drinks.First(d => d.Id == dt.DrinkID) != null),
                IsPaid = q.IsPaid,
                Amount = q.Amount,
                PaymentMethod = q.PaymentMethod.ToString()
            });
        }

        //Post api/Order
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] OrderViewModel value)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var today = DateTime.Today;
                    var orderID = Guid.NewGuid();
                    float amount = new float();
                    List<Dish> dishes = new List<Dish>();
                    List<Drink> drinks = new List<Drink>();
                    List<DishTask> dishTasks = new List<DishTask>();
                    List<DrinkTask> drinkTasks = new List<DrinkTask>();
                    var time = DateTime.Now.TimeOfDay;
                    var shift = _context.Shifts.First(s => s.From <= time && s.To > time);


                    foreach (var el in value.Dishes)
                    {
                        var dish = await _context.LazyLoadDish(el.Name);
                        if (dish == null) return NotFound(new { status = "DishNotFound" });
                        amount += dish.Amount;
                        var day = _context.Days.Where(d => d.Date == today.ToString("yyyy-MM-dd")).Where(d => d.Position == dish.Position).First(d => d.Shift == shift.Code);
                        if (day == null) return NotFound(new { status = "DayNotFound" });
                        var worker = _context.Workers.First(w => w.Name == day.Worker_name);
                        if (worker == null) return NotFound(new { status = "WorkerNotFound" });
                        var dishTask = new DishTask { DishTaskID = Guid.NewGuid(), DishID = dish.DishID, OrderID = orderID, WorkerID = worker.WorkerID, Status = Status.ToDo };
                        //dish.Tasks.Add(dishTask);
                        dishTasks.Add(dishTask);
                    }
                    foreach (var el in value.Drinks)
                    {
                        var drink = await _context.LazyLoadDrink(el.Name);
                        if (drink == null) return NotFound(new { status = "DrinkNotFound" });
                        amount += drink.Amount;
                        var day = _context.Days.Where(d => d.Date == today.ToString("yyyy-MM-dd")).Where(d => d.Position == Location.FirstRoom.ToString()).First(d => d.Shift == shift.Code);
                        if (day == null) return NotFound(new { status = "DayNotFound" });
                        var worker = _context.Workers.First(w => w.Name == day.Worker_name);
                        if (worker == null) return NotFound(new { status = "WorkerNotFound" });
                        var drinkTask = new DrinkTask { DrinkTaskID = Guid.NewGuid(), DrinkID = drink.Id, OrderID = orderID, WorkerID = worker.WorkerID, Status = Status.ToDo };
                        //drink.Tasks.Add(drinkTask);
                        drinkTasks.Add(drinkTask);
                    }
                    var table = await _context.Tables.FirstAsync(q => q.Number == value.Table);
                    var paymentMethod = (PaymentMethod)System.Enum.Parse(typeof(PaymentMethod), value.PaymentMethod);
                    if (table == null) return NotFound(new { status = "TableNotFound" });


                    var newOrder = new Order
                    {
                        OrderID = orderID,
                        Table = table,
                        IsPaid = false,
                        PaymentMethod = paymentMethod,
                        Amount = amount,
                        Status = Status.ToDo,
                        DishTasks = dishTasks,
                        DrinkTasks = drinkTasks
                    };

                    _context.Orders.Add(newOrder);
                    _context.Entry(newOrder).State = EntityState.Added;
                    await _context.SaveChangesAsync();
                  

                    return CreatedAtAction("GetOrder", new { id = newOrder.OrderID }, newOrder);
                }
                else
                {
                    return BadRequest(new { status = "ModelState is invalid" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return BadRequest(new { status = ex.ToString() });
            }
        }
        // PUT api/Order/{id}/{status}
        [HttpPut("{OrderID}/{Status}")]
        public async Task<IActionResult> Update([FromRoute] Guid OrderID, [FromRoute] int Status)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Order origin = await _context.LazyLoadOrder(OrderID);
                    if (origin == null) return NotFound(new { status = "notFound" });
                    origin.Status = (Status)Status;
                    _context.Orders.Attach(origin);
                    _context.Entry(origin).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return Ok(new { status = "updated" });
                }
                else
                {
                    return BadRequest(new { status = "failure" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound(new { status = "notFound" });
            }
        }


        // PUT api/Order/{id}
        [HttpPut("{OrderID}")]
        public async Task<IActionResult> Update([FromRoute] Guid OrderID, [FromBody] Order value)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Order origin = await _context.LazyLoadOrder(OrderID);
                    if (origin == null) return NotFound(new { status = "notFound" });
                    origin = value;
                    origin.OrderID = OrderID;
                    _context.Orders.Attach(origin);
                    _context.Entry(origin).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    await _context.SaveChangesAsync();
                    return Ok(new { status = "updated" });
                }
                else
                {
                    return BadRequest(new { status = "failure" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                return NotFound(new { status = "notFound" });
            }
        }

        // DELETE: api/Order/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var order = await _context.Orders.SingleOrDefaultAsync(m => m.OrderID == id);
            if (order == null)
            {
                return NotFound();
            }

            _context.Orders.Remove(order);
            _context.Entry(order).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

            return Ok(order);
        }

        // DELETE: api/Order
        [HttpDelete]
        public async Task<IActionResult> DeleteOrders()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            foreach(var order in _context.Orders)
            {
                _context.Orders.Remove(order);
            }
            
            await _context.SaveChangesAsync();

            return Ok(new { status = "removed" });
        }

        private bool OrderExists(Guid id)
        {
            return _context.Orders.Any(e => e.OrderID == id);
        }
    }
}