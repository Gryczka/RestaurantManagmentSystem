﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class DrinkOrderConnector
    {
        public Guid DrinkID { get; set; }
        public Guid OrderID { get; set; }
        public virtual Drink Drink { get; set; }
        public virtual Order Order { get; set; }
    }
}
