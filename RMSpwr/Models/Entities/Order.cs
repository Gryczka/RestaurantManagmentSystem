﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class Order
    {
        [Key]
        public Guid OrderID { get; set; }
        public Table Table { get; set; }
        //public List<DishOrderConnector> Dishes { get; set; }
        //public List<DrinkOrderConnector> Drinks { get; set; }
        public Boolean IsPaid { get; set; }
        public float Amount { get; set; }
        public PaymentMethod? PaymentMethod { get; set; }
        public Status? Status { get; set; }

        public virtual ICollection<DishTask> DishTasks { get; set; }
        public virtual ICollection<DrinkTask> DrinkTasks { get; set; }
    }

    public enum PaymentMethod
    {
        Card, Cash, RoomAccount
    }
}
