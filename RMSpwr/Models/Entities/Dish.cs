﻿using RMSpwr.Models.Entities.Graphic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class Dish
    {
        [Key]
        public Guid DishID { get; set; }
        //public List<DishOrderConnector> Orders { get; set; }
        public string Name { get; set; }
        public float Amount { get; set; }
        public List<Component> Components { get; set; }
        public string Description { get; set; }
        public AdditionalDishInformation? AdditionalInformation { get; set; }
        public string Position { get; set; }

        public virtual ICollection<DishTask> Tasks { get; set; }
    }

    public enum AdditionalDishInformation
    {
        None, Vegetarian, Vegan, Spicy
    }
}
