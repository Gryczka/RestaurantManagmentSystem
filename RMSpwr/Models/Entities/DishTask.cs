﻿using RMSpwr.Models.Entities.Graphic;
using RMSpwr.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class DishTask
    {
        [Key]
        public Guid DishTaskID { get; set; }
        [ForeignKey("Dish")]
        public Guid DishID { get; set; }
        [ForeignKey("Order")]
        public Guid OrderID { get; set; }
        [ForeignKey("Worker")]
        public Guid WorkerID { get; set; }
        public Status? Status { get; set; }

        public virtual Dish Dish { get; set; }
        public virtual Order Order { get; set; }
        public virtual Worker Worker { get; set; }

        public DishTaskJson ToJson()
        {
            return new DishTaskJson
            {
                DishID = this.DishID,
                Dish_name = this.Dish.Name,
                Worker_name = this.Worker.Name,
                Status = this.Status
            };
        }
    }

    public class DishTaskJson
    {
        public Guid DishID { get; set; }
        public string Dish_name { get; set; }
        public string Worker_name { get; set; }
        public Status? Status { get; set; }
    }

    public enum Status
    {
        ToDo, InProgress, Done
    }
}
