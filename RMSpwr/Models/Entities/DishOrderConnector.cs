﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class DishOrderConnector
    { // normalnie EF robił tablice asocjacyjne sam ale w Core stwierdzili że po co, sam sobie pisz
        public Guid DishID { get; set; }
        public Guid OrderID { get; set; }
        public virtual Dish Dish { get; set; }
        public virtual Order Order { get; set; }
    }
}
