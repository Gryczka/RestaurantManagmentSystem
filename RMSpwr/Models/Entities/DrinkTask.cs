﻿using RMSpwr.Models.Entities.Graphic;
using RMSpwr.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class DrinkTask
    {
        [Key]
        public Guid DrinkTaskID { get; set; }
        [ForeignKey("Drink")]
        public Guid DrinkID { get; set; }
        [ForeignKey("Order")]
        public Guid OrderID { get; set; }
        [ForeignKey("Worker")]
        public Guid WorkerID { get; set; }
        public Status? Status { get; set; }

        public virtual Drink Drink { get; set; }
        public virtual Order Order { get; set; }
        public virtual Worker Worker { get; set; }

        public DrinkTaskJson ToJson()
        {
            return new DrinkTaskJson
            {
                DrinkID = this.DrinkID,
                Drink_name = this.Drink.Name,
                Worker_name = this.Worker.Name,
                Status = this.Status
            };
        }
    }

    public class DrinkTaskJson
    {
        public Guid DrinkID { get; set; }
        public string Drink_name { get; set; }
        public string Worker_name { get; set; }
        public Status? Status { get; set; }
    }
}
