﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    [JsonObject]
    [Serializable]
    public class GraphicInfo
    {
        [Key]
        public string Graphic_id { get; set; }
        public string Name { get; set; }
    }
}
