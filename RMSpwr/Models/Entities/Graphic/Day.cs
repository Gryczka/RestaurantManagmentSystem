﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    public class Day
    {
        [Key]
        public Guid DayID { get; set; }
        public string Date { get; set; }
        public string Position { get; set; }
        public string Shift { get; set; }
        public string Worker_name { get; set; }
    }
}
