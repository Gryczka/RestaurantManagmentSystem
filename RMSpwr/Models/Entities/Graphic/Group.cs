﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    [JsonObject]
    [Serializable]
    public class Group
    {
        [Key]
        public Guid GroupID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Count_of_person { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
        public virtual ICollection<Worker> Workers { get; set; }
    }
}
