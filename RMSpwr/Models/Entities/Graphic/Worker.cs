﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    [JsonObject]
    [Serializable]
    public class Worker
    {
        [Key]
        public Guid WorkerID { get; set; }
        public string Name { get; set; }
        [ForeignKey("Group")]
        public Guid GroupID { get; set; }
        public float WorkingTime { get; set; }

        public virtual Group Group { get; set; }
    }
}
