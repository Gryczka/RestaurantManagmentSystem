﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    public class DaysList
    {
        public List<Day> Days { get; set; }
    }
}
