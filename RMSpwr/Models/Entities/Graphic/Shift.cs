﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    [JsonObject]
    [Serializable]
    public class Shift
    {
        [Key]
        public Guid ShiftID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
    }
}
