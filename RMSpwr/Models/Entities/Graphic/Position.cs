﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    [JsonObject]
    [Serializable]
    public class Position
    {
        [Key]
        public Guid PositionID { get; set; }
        public string Name { get; set; }
        [ForeignKey("Group")]
        public Guid GroupID { get; set; }
        public int Count_of_person { get; set; }

        public virtual Group Group { get; set; }
    }
}
