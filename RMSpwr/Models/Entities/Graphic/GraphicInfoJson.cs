﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities.Graphic
{
    [JsonObject]
    [Serializable]
    public class GenerateGraphicJson
    {
        public string graphic_id { get; set; }
        public string date { get; set; }
    }

    [JsonObject]
    [Serializable]
    public class GraphicInfoJson
    {
        public GraphicInfo Graphic_info { get; set; }
        public List<Group> Groups { get; set; }
        public List<ShiftsJson> Shifts { get; set; }
        public List<WorkersJson> Workers { get; set; }
        public List<PositionsJson> Positions { get; set; }
    }

    [JsonObject]
    [Serializable]
    public class ShiftsJson
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
    }

    [JsonObject]
    [Serializable]
    public class WorkersJson
    {
        public string Name { get; set; }
        public string Group { get; set; }
        public WorkingTime Working_time { get; set; }
        public string Position { get; set; }
    }

    [JsonObject]
    [Serializable]
    public class WorkingTime
    {
        public float Time { get; set; }
    }

    [JsonObject]
    [Serializable]
    public class PositionsJson
    {
        public string Name { get; set; }
        public string Group { get; set; }
        public int Count_of_person { get; set; }
    }
}
