﻿using RMSpwr.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Concrete
{
    public class OrderViewModel
    {
        public string Table { get; set; }
        public List<Element> Dishes { get; set; }
        public List<Element> Drinks { get; set; }
        public string PaymentMethod { get; set; }
    }

    public class Element
    {
        public string Name { get; set; }
    }
}
