﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RMSpwr.Models.Entities;
using RMSpwr.Models.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace RMSpwr.Models.Helpers
{
    public static class EntityHelper
    {
        public static async Task<List<Dish>> LazyLoadDishes(this OrderContext context)
        {
            return await context.Dishes.Include(p => p.Name).Include(p => p.Amount).Include(p => p.Components).Include(p => p.Description).ToListAsync();
        }

        public static async Task<Dish> LazyLoadDish(this OrderContext context, string name)
        {
            return await context.Dishes.Include(p => p.Components).FirstAsync(q => q.Name == name);
        }

        public static async Task<List<Component>> LazyLoadComponents(this OrderContext context)
        {
            return await context.Components.Include(p => p.Element).ToListAsync();
        }

        public static async Task<List<Drink>> LazyLoadDrinks(this OrderContext context)
        {
            return await context.Drinks.Include(p => p.Name).Include(p => p.Amount).Include(p => p.Components).Include(p => p.Description).ToListAsync();
        }

        public static async Task<Drink> LazyLoadDrink(this OrderContext context, string name)
        {
            return await context.Drinks.Include(p => p.Components).FirstAsync(q => q.Name == name);
        }

        public static async Task<List<Order>> LazyLoadOrders(this OrderContext context)
        {
            return await context.Orders.Include(p => p.Table).Include(p => p.DishTasks).Include(p => p.DrinkTasks).ToListAsync();
        }

        public static async Task<Order> LazyLoadOrder(this OrderContext context, Guid Id)
        {
            return await context.Orders.Include(p => p.Table).Include(p => p.DishTasks).Include(p => p.DrinkTasks).Include(p => p.IsPaid).Include(p => p.Amount)
                                .Include(p => p.PaymentMethod).FirstAsync(q => q.OrderID == Id);
        }

        public static async Task<DrinkTask> LazyLoadDrinkTask(this OrderContext context, Guid drinkTaskID)
        {
            return await context.DrinkTasks.Include(p => p.Status).FirstAsync(q => q.DrinkTaskID == drinkTaskID);
        }
    }
}
