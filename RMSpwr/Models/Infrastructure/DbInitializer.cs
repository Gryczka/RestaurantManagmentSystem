﻿using RMSpwr.Models.Entities;
using RMSpwr.Models.Entities.Graphic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Infrastructure
{
    public class DbInitializer
    {
        public static void Initialize(OrderContext context)
        {
            if (!context.GraphicInfos.Any())
            {
                var graphicInfos = new GraphicInfo[]
                {
                    new GraphicInfo{ Graphic_id = "RMS", Name = "RestaurantManagmentSystem"}
                };
                foreach (var graphciInfo in graphicInfos)
                {
                    context.GraphicInfos.Add(graphciInfo);
                }
                context.SaveChanges();
            }
            if (!context.Groups.Any())
            {
                var groups = new Group[]
                {
                    new Group{ GroupID = Guid.NewGuid(), Code = "Kuchna", Name = "Kucharze", Count_of_person = 6 },
                    new Group{ GroupID = Guid.NewGuid(), Code = "Sala", Name = "Kelnerki", Count_of_person = 6 },
                };
                foreach (var group in groups)
                {
                    context.Groups.Add(group);
                }
                context.SaveChanges();
            }
            if (!context.Shifts.Any())
            {
                var shifts = new Shift[]
                {
                    new Shift{ ShiftID = Guid.NewGuid(), Code = "Zmiana1", Description = "8-16", From = new TimeSpan(), To = new TimeSpan() },
                    new Shift{ ShiftID = Guid.NewGuid(), Code = "Zmiana2", Description = "16-24", From = new TimeSpan() , To = new TimeSpan() },
                };
                foreach (var shift in shifts)
                {
                    context.Shifts.Add(shift);
                }
                context.SaveChanges();
            }
            if (!context.Positions.Any())
            {
                var group_kuchnia = context.Groups.FirstOrDefault(q => q.Code == "Kuchna").GroupID;
                var group_sala = context.Groups.FirstOrDefault(q => q.Code == "Sala").GroupID;
                var positions = new Position[]
                {
                    new Position{ PositionID = Guid.NewGuid(), Name = "Mieso", GroupID = group_kuchnia, Count_of_person = 1 },
                    new Position{ PositionID = Guid.NewGuid(), Name = "Makarony", GroupID = group_kuchnia, Count_of_person = 1 },
                    new Position{ PositionID = Guid.NewGuid(), Name = "Wydawka", GroupID = group_kuchnia, Count_of_person = 1 },
                    new Position{ PositionID = Guid.NewGuid(), Name = Location.FirstRoom.ToString(), GroupID = group_sala, Count_of_person = 1 },
                    new Position{ PositionID = Guid.NewGuid(), Name = Location.SecondRoom.ToString(), GroupID = group_sala, Count_of_person = 1 },
                    new Position{ PositionID = Guid.NewGuid(), Name = Location.Tarrace.ToString(), GroupID = group_sala, Count_of_person = 1 },
                    new Position{ PositionID = Guid.NewGuid(), Name = Location.Balcony.ToString(), GroupID = group_sala, Count_of_person = 1 },
                    new Position{ PositionID = Guid.NewGuid(), Name = "Bar", GroupID = group_sala, Count_of_person = 1 },
                };
                foreach (var position in positions)
                {
                    context.Positions.Add(position);
                }
                context.SaveChanges();
            }
            if (!context.Workers.Any())
            {
                var group_kuchnia = context.Groups.FirstOrDefault(q => q.Code == "Kuchna").GroupID;
                var group_sala = context.Groups.FirstOrDefault(q => q.Code == "Sala").GroupID;

                var workers = new Worker[]
                {
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Han Solo", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Anakin Skywalker", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Luke Skywalker", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Obi-Wan Kenobi", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Palpatine", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Leia Organa", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Chewbacca", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Padmé Amidala", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "C-3PO", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "R2-D2", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Yoda", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Rey", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Finn", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Kylo Ren", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Leia Organa2", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Chewbacca2", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Padmé Amidala2", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "C-3PO2", GroupID = group_kuchnia, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "R2-D2", Group = group_kuchnia, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Yoda", Group = group_kuchnia, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Rey", Group = group_kuchnia, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Finn", Group = group_kuchnia, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Kylo Ren", Group = group_kuchnia, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Gilderoy Lockhart", Group = group_kuchnia, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Lucjusz Malfoy", Group = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Zgredek", Group = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Artur Weasley", Group = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "Korneliusz Knot", Group = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Jęcząca Marta", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Aragog", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Harry Potter", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Hermiona Granger", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Ron Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Ginny Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Fred Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "George Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Percy Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Charlie Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "William Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Korneliusz Knot", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Artur Weasley", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Zgredek", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Lucjusz Malfoy", GroupID = group_sala, WorkingTime = 1.0f  },
                    new Worker { WorkerID = Guid.NewGuid(), Name = "Gilderoy Lockhart", GroupID = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "", Group = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "BB-8", Group = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "BB-8", Group = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "BB-8", Group = group_sala, WorkingTime = 1.0f  },
                    //new Worker { WorkerID = Guid.NewGuid(), Name = "BB-8", Group = group_sala, WorkingTime = 1.0f  },
                };
                foreach (var worker in workers)
                {
                    context.Workers.Add(worker);
                }
                context.SaveChanges();
            }



            if (!context.Components.Any())
            {
                var components = new Component[]
                {
                    new Component{Id=Guid.NewGuid(),Element="Szynka parmeńska"},
                    new Component{Id=Guid.NewGuid(),Element="Boczniak królewski"},
                    new Component{Id=Guid.NewGuid(),Element="Orzechy laskowe"},
                    new Component{Id=Guid.NewGuid(),Element="Cebula kalabryjska"},
                    new Component{Id=Guid.NewGuid(),Element="Pietruszka"},
                    new Component{Id=Guid.NewGuid(),Element="Masło"},
                    new Component{Id=Guid.NewGuid(),Element="Parmigiano-Reggiano DOP"},
                    new Component{Id=Guid.NewGuid(),Element="Sos z gorgonzoli"},
                    new Component{Id=Guid.NewGuid(),Element="Sos pomidorowy"},
                    new Component{Id=Guid.NewGuid(),Element="Wołowina Fassona Piemontese"},
                    new Component{Id=Guid.NewGuid(),Element="Pecorino Romano DOP"},
                };
                foreach (var component in components)
                {
                    context.Components.Add(component);
                }
                context.SaveChanges();
            }

            if (!context.Dishes.Any())
            {
                var dishes = new Dish[]
                {
                    new Dish{DishID=Guid.NewGuid(),Name="Gnocchi",Amount=27.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDishInformation.None,
                    Position = "Makarony"},
                    new Dish{DishID=Guid.NewGuid(),Name="Bigoli",Amount=31.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDishInformation.None,
                    Position = "Mieso"},
                };
                foreach (var dish in dishes)
                {
                    context.Dishes.Add(dish);
                }
                context.SaveChanges();
            }

            if (!context.Drinks.Any())
            {
                var drinks = new Drink[]
                {
                    new Drink{Id=Guid.NewGuid(),Name="Wino Białe",Amount=12.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDrinkInformation.WithAlcohol, Position = "Bar"},
                    new Drink{Id=Guid.NewGuid(),Name="Wino Czerwone",Amount=15.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDrinkInformation.WithAlcohol, Position = "Bar"},
                    new Drink{Id=Guid.NewGuid(),Name="Herbata z rumem",Amount=14.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDrinkInformation.WithAlcohol, Position = "Bar"},
                };
                foreach (var drink in drinks)
                {
                    context.Drinks.Add(drink);
                }
                context.SaveChanges();
            }

            if (!context.Tables.Any())
            {
                var tables = new Table[]
                {
                    new Table{Id=Guid.NewGuid(),Number="F1",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F2",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F3",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F4",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F5",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F6",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F7",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="S1",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S2",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S3",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S4",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S5",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S6",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S7",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="T1",Location=Location.Tarrace},
                    new Table{Id=Guid.NewGuid(),Number="T2",Location=Location.Tarrace},
                    new Table{Id=Guid.NewGuid(),Number="T3",Location=Location.Tarrace},
                    new Table{Id=Guid.NewGuid(),Number="B1",Location=Location.Balcony},
                    new Table{Id=Guid.NewGuid(),Number="B2",Location=Location.Balcony},
                    new Table{Id=Guid.NewGuid(),Number="B3",Location=Location.Balcony},
                    new Table{Id=Guid.NewGuid(),Number="B4",Location=Location.Balcony},
                };
                foreach (var table in tables)
                {
                    context.Tables.Add(table);
                }
                context.SaveChanges();
            }
        }
    }
}
