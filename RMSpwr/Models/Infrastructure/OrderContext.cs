﻿using Microsoft.EntityFrameworkCore;
using RMSpwr.Models.Entities;
using RMSpwr.Models.Entities.Graphic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace RMSpwr.Models.Infrastructure
{
    public class OrderContext : DbContext
    {
        public OrderContext(DbContextOptions<OrderContext> options)
            : base(options)
        {
        }

        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Drink> Drinks { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<Component> Components { get; set; }
        public DbSet<DrinkTask> DrinkTasks { get; set; }
        public DbSet<DishTask> DishTasks { get; set; }
        //public DbSet<DishOrderConnector> DishOrderConnectors { get; set; }
        //public DbSet<DrinkOrderConnector> DrinkOrderConnectors { get; set; }

        public DbSet<GraphicInfo> GraphicInfos { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Worker> Workers { get; set; }

        public DbSet<Day> Days { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        { //budowanie zależności
            //builder.Entity<DishOrderConnector>()
            // .HasOne(q => q.Order)
            // .WithMany(q => q.Dishes)
            // .HasForeignKey(q => q.OrderID);

            //builder.Entity<DishOrderConnector>()
            //  .HasOne(q => q.Dish)
            //  .WithMany(q => q.Orders)
            //  .HasForeignKey(q => q.DishID);

            //builder.Entity<DishOrderConnector>()
            //  .HasKey(q => new { q.OrderID, q.DishID });

            //builder.Entity<DrinkOrderConnector>()
            //    .HasOne(q => q.Order)
            //    .WithMany(q => q.Drinks)
            //    .HasForeignKey(q => q.OrderID);

            //builder.Entity<DrinkOrderConnector>()
            //  .HasOne(q => q.Drink)
            //  .WithMany(q => q.Orders)
            //  .HasForeignKey(q => q.DrinkID);

            //builder.Entity<DrinkOrderConnector>()
            //  .HasKey(q => new { q.OrderID, q.DrinkID });

            //builder.Entity<Position>()
            //            .HasOne(m => m.Group)
            //            .WithMany(t => t.Positions)
            //            .HasForeignKey(m => m.GroupID);

            //builder.Entity<Position>()
            //    .HasRequired(c => c.Group)
            //    .WithMany()
            //    .WillCascadeOnDelete(false);

            base.OnModelCreating(builder);
            //builder.Entity<Position>().HasMany(i => i).WithRequired().WillCascadeOnDelete(false);
        }
    }
}
