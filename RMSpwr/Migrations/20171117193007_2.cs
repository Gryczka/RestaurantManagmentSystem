﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RMSpwr.Migrations
{
    public partial class _2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "GroupID",
                table: "Positions",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Positions_GroupID",
                table: "Positions",
                column: "GroupID");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Groups_GroupID",
                table: "Positions",
                column: "GroupID",
                principalTable: "Groups",
                principalColumn: "GroupID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Groups_GroupID",
                table: "Positions");

            migrationBuilder.DropIndex(
                name: "IX_Positions_GroupID",
                table: "Positions");

            migrationBuilder.DropColumn(
                name: "GroupID",
                table: "Positions");
        }
    }
}
