﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RMSpwr.Migrations
{
    public partial class _8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DishOrderConnectors");

            migrationBuilder.DropTable(
                name: "DrinkOrderConnectors");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DishTasks",
                columns: table => new
                {
                    DishTaskID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DishID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: true),
                    WorkerID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DishTasks", x => x.DishTaskID);
                    table.ForeignKey(
                        name: "FK_DishTasks_Dishes_DishID",
                        column: x => x.DishID,
                        principalTable: "Dishes",
                        principalColumn: "DishID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DishTasks_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DishTasks_Workers_WorkerID",
                        column: x => x.WorkerID,
                        principalTable: "Workers",
                        principalColumn: "WorkerID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DrinkTasks",
                columns: table => new
                {
                    DrinkTaskID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DrinkID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrderID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: true),
                    WorkerID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrinkTasks", x => x.DrinkTaskID);
                    table.ForeignKey(
                        name: "FK_DrinkTasks_Drinks_DrinkID",
                        column: x => x.DrinkID,
                        principalTable: "Drinks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrinkTasks_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrinkTasks_Workers_WorkerID",
                        column: x => x.WorkerID,
                        principalTable: "Workers",
                        principalColumn: "WorkerID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DishTasks_DishID",
                table: "DishTasks",
                column: "DishID");

            migrationBuilder.CreateIndex(
                name: "IX_DishTasks_OrderID",
                table: "DishTasks",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_DishTasks_WorkerID",
                table: "DishTasks",
                column: "WorkerID");

            migrationBuilder.CreateIndex(
                name: "IX_DrinkTasks_DrinkID",
                table: "DrinkTasks",
                column: "DrinkID");

            migrationBuilder.CreateIndex(
                name: "IX_DrinkTasks_OrderID",
                table: "DrinkTasks",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_DrinkTasks_WorkerID",
                table: "DrinkTasks",
                column: "WorkerID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DishTasks");

            migrationBuilder.DropTable(
                name: "DrinkTasks");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Orders");

            migrationBuilder.CreateTable(
                name: "DishOrderConnectors",
                columns: table => new
                {
                    OrderID = table.Column<Guid>(nullable: false),
                    DishID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DishOrderConnectors", x => new { x.OrderID, x.DishID });
                    table.ForeignKey(
                        name: "FK_DishOrderConnectors_Dishes_DishID",
                        column: x => x.DishID,
                        principalTable: "Dishes",
                        principalColumn: "DishID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DishOrderConnectors_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DrinkOrderConnectors",
                columns: table => new
                {
                    OrderID = table.Column<Guid>(nullable: false),
                    DrinkID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrinkOrderConnectors", x => new { x.OrderID, x.DrinkID });
                    table.ForeignKey(
                        name: "FK_DrinkOrderConnectors_Drinks_DrinkID",
                        column: x => x.DrinkID,
                        principalTable: "Drinks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrinkOrderConnectors_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DishOrderConnectors_DishID",
                table: "DishOrderConnectors",
                column: "DishID");

            migrationBuilder.CreateIndex(
                name: "IX_DrinkOrderConnectors_DrinkID",
                table: "DrinkOrderConnectors",
                column: "DrinkID");
        }
    }
}
