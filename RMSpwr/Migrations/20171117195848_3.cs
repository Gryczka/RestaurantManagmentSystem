﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RMSpwr.Migrations
{
    public partial class _3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "PositionID",
                table: "Workers",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Workers_PositionID",
                table: "Workers",
                column: "PositionID");

            migrationBuilder.AddForeignKey(
                name: "FK_Workers_Positions_PositionID",
                table: "Workers",
                column: "PositionID",
                principalTable: "Positions",
                principalColumn: "PositionID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workers_Positions_PositionID",
                table: "Workers");

            migrationBuilder.DropIndex(
                name: "IX_Workers_PositionID",
                table: "Workers");

            migrationBuilder.DropColumn(
                name: "PositionID",
                table: "Workers");
        }
    }
}
